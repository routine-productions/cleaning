Head_Menu();
function Head_Menu() {

    var Pathname = location.pathname;

    Pathname = Pathname.slice(1);

    if (Pathname == "") {
        $(".Header-Block.Menu li:first-of-type").addClass('Active');
        $('.Header-Logo').removeAttr('href');

    } else {
        $('.Header-Block.Menu a[href = ' + Pathname + ']').parents('li').addClass('Active');
        $(".Header-Logo").addClass("Active");
        $(".Header-Back").addClass("Active");
    }
}


$('.Header-Block.Menu li').click(function () {

    $(this).css('color', '#e9379c').siblings().css('color', "");

    if ($(this).hasClass("Active")) {
        $(this).css('color', "");
    }
});


$(".Contacts-Form button").hover(function () {

        $('.Contacts-Form input, .Contacts-Form textarea').each(function (index, element) {
            if ($(element).val() == '') {
                $(".Contacts-Form button").attr('title', 'Заполните все поля!');
                return false;
            } else {
                $(".Contacts-Form button").attr('title', '');
            }

        });
    },

    function () {
        $(".Contacts-Form button").attr('title', '');
    }
);


$("input,textarea").keyup(function () {

        $('.Contacts-Form input, .Contacts-Form textarea').each(function (index, element) {
            if ($(element).val() == '') {

                $(".Contacts-Form button").removeClass('Active');
                return false;
            } else {

                $(".Contacts-Form button").addClass('Active');
            }

        });
    }
);
