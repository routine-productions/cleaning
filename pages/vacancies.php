<div class="Demonstration-Images">
    <div class="Demonstration-Images">
        <div class="Demonstration-Block">
            <img src="/images/main-pic-1.jpg" alt="">
        </div>
        <div class="Demonstration-Block">
            <img src="/images/main-pic-2.jpg" alt="">
        </div>
        <div class="Demonstration-Block">
            <img src="/images/main-pic-3.jpg" alt="">
        </div>
    </div>
</div>


<div class="Description-Wrapper Vacancies">
    <h2>Вакансии</h2>

    <p>В связи с расширением сферы деятельности наша компания предлагает трудоустройство по следующим вакансиям:</p>

    <div class="Description-Block Block-Left">
        <ul>
            <li>менеджер объекта</li>
            <li>менеджер по продажам</li>
            <li>оператор уборки (уборщик)</li>
            <li>оператор уборки (уборщик) в мобильную бригаду (с обучением)</li>
            <li>грузчик</li>
        </ul>
    </div>
    <div class="Description-Block Block-Right">
        <ul>
            <li>кухонный работник (посудомойщик)</li>
            <li>помощник повара</li>
            <li>кассир</li>
            <li>работник торгового зала</li>
            <li>разнорабочий</li>
            <li>дворник</li>
        </ul>
    </div>

    <div class="Description-Block Full-Width">
        <h3>мы гарантируем</h3>
        <ul>
            <li>трудоустройство в соответствии с ТК РФ</li>
            <li>участие в социальных государственных программах по трудоустройству</li>
            <li>обучение/ при необходимости/ на рабочем месте технологии уборки, умелому пользованию спец.
                оборудованием, инвентарем и хим.средствами;
            </li>
            <li>достойная заработная плата;</li>
            <li>графики работы в соответствии с графиком работы обслуживаемого объекта и Вашим желанием заработать (2/2;
                3/3; 3/1, 5/2, неполный день)
            </li>
        </ul>
    </div>
</div>
