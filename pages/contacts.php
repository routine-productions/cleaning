<div class="Demonstration-Images">
    <div class="Demonstration-Block Width-Full JS-Image-Align" data-image-ratio='auto'
         data-image-position='center/top'>
        <img src="/images/main-pic-5.jpg" alt="">
    </div>
</div>

<section class="Contacts Page-Contact">

    <h2>обратная связь</h2>

    <div class="Contacts-Form">
        <form class='JS-Form'
              data-form-email='clining@mail.ru, slugba_uborki@mail.ru'
              data-form-subject='Order form website'
              data-form-url='./js/data.form.php'
              data-form-method='POST'>
            <div class="Contacts-Dispatch">
                <span>Имя</span>
                <input class="JS-Form-Require" type='text' name='name' data-input-title='Your first name'>
            </div>
            <div class="Contacts-Dispatch">
                <span>Телефон или Email</span>
                <input class="JS-Form-Require" type='text' name='contact' data-input-title='Your contact'>
            </div>
            <div class="Contacts-Dispatch">
                <span>Сообщение</span>
                <textarea class="JS-Form-Require" name='message' data-input-title='Your message'></textarea>
            </div>

            <button title="" class="JS-Form-Button">Отправить</button>

            <div class="JS-Form-Result"></div>
        </form>

        <div class="Contacts-Info Right">
            <dl class="address">
                <dt>Адрес</dt>
                <dd> ул. Электрозаводская, дом 1<br>
                     г. Владимир
                </dd>
            </dl>
            <dl class="tel">
                <dt>Телефоны</dt>
                <dd><a href="tel:+8(4922)37-39-11"> +8&nbsp;(4922)&nbsp;37&#8209;39&#8209;11</a></dd>
            </dl>
            <dl class="email">
                <dt>E-mail:</dt>
                <dd>
                    <a href="mailto:clining@mail.ru">clining@mail.ru</a><br>
                    <a href="mailto:slugba_uborki@mail.ru">slugba_uborki@mail.ru</a>
                </dd>
            </dl>
        </div>
    </div>


    <div class="Map">
        <script type="text/javascript" charset="utf-8"
                src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=Xm0JMc0VFo1ZvMQp7_EXJz39U_KZiRoV&width=100%&height=400&lang=ru_UA&sourceType=constructor"></script>
    </div>

   <?php
    require __DIR__ . "/../modules/contacts-info.php"
   ?>
</section>