<div class="Main-Wrapper">
    <div class="Demonstration-Images">
        <div class="Demonstration-Block Width-Full JS-Image-Align" data-image-ratio='auto'
             data-image-position='center/top'>
            <img src="/images/main-pic-4.jpg" alt="">
            <div class="Demonstration-Description">
                <p class="Demonstration-Text"><span></span>Качественная уборка помещений любой площади во Владимире</p>
                <button class="See-Services">
                    <a href="services">
                        Посмотреть наши услуги
                    </a>
                </button>
            </div>
        </div>
    </div>
    <section class="Main-Vacancies">
        <h2>услуги компании</h2>

        <ul class="Vacancies-Items">
            <li class="Vacancies-Item House">
                <?php require_once $Dir_Images . "house.svg"; ?>
                <h3>Дома и котеджи</h3>
            </li>
            <li class="Vacancies-Item Office">
                <?php require_once $Dir_Images . "office.svg"; ?>
                <h3>Офисные помещения</h3>
            </li>
            <li class="Vacancies-Item Industrial">
                <?php require_once $Dir_Images . "industrial.svg"; ?>
                <h3>Промышленные помещения</h3>
            </li>
            <li class="Vacancies-Item Climbing">
                <?php require_once $Dir_Images . "climbing.svg"; ?>
                <h3>Промышленный альпинизм</h3>
            </li>
            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "house2.svg"; ?>
                <h3>Уборка территорий</h3>
            </li>
            <li class="Vacancies-Item Washingmachine">
                <?php require_once $Dir_Images . "washingmachine.svg"; ?>
                <h3>Xимчистка</h3>
            </li>


            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/aquarium.svg"; ?>
                <h3>Чистка аквариумов</h3>
            </li>

            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/banner-ad-removal.svg"; ?>
                <h3>Удаление наружной рекламы</h3>
            </li>


            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/beach-cleaning.svg"; ?>
                <h3>Очистка береговой полосы</h3>
            </li>

            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/gardening.svg"; ?>
                <h3>Подрезка деревьев и кустарников</h3>
            </li>

            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/pool.svg"; ?>
                <h3>Очистка бассейнов и водоемов</h3>
            </li>

            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/shop.svg"; ?>
                <h3>Магазин</h3>
            </li>

            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/staff.svg"; ?>
                <h3>Услуги персонала</h3>
            </li>

            <li class="Vacancies-Item House2">
                <?php require_once $Dir_Images . "services/technics.svg"; ?>
                <h3>Спецтехника</h3>
            </li>

        </ul>
    </section>
    <section class="Advantages">
        <h2>наши преймущества</h2>

        <div class="Advantages-Content">
            <ul class="Advantages-Items">
                <li class="Advantages-Item Vacuumcleaner">
                    <?php
                    require_once $Dir_Images . "vacuumcleaner.svg";
                    ?>
                    <h3>современная техника</h3>
                </li>
                <li class="Advantages-Item Worker">
                    <?php
                    require_once $Dir_Images . "worker.svg";
                    ?>
                    <h3>надежный персонал</h3>
                </li>
                <li class="Advantages-Item Certificate">
                    <?php
                    require_once $Dir_Images . "certificate.svg";
                    ?>
                    <h3>гарантрованное качество</h3>
                </li>
            </ul>
            <p>
                Все работы проводятся с использованием современных специальных средств и в соответствии с технологиями,
                позволяющими не только очищать загрязнения, но и сохранять поверхность мебели и полов в превосходном
                виде. «Городская служба уборки» обеспечивает гарантированно качественный сервис обслуживания.
            </p>
        </div>
    </section>
    <section class="Contacts">
        <h2>контактные данные</h2>
        <div class="Map">
            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=Xm0JMc0VFo1ZvMQp7_EXJz39U_KZiRoV&width=100%&height=400&lang=ru_UA&sourceType=constructor"></script>
        </div>
        <?php
        require __DIR__ . "/../modules/contacts-info.php"
        ?>
    </section>
</div>