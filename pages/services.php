<div class="Services">
    <h2>Услуги компании</h2>

    <ul class="Services-List">
        <li class="Services-Item">
            <a href="service-1" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-1.jpg" alt="">

                <p>Промышленный клиниг<br>
                    и обслуживание производственных<br>
                    и складских помещений</p>
            </a>
        </li>
        <li class="Services-Item">
            <a href="service-2" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-2.jpg" alt="">

                <p>Ежедневная уборка и обслуживание <br>
                    офисов, торговых центров, <br>
                    супермаркетов</p>
            </a>
        </li>
        <li class="Services-Item">
            <a href="service-3" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-3.jpg" alt="">

                <p>
                    Генеральная уборка и после-<br>
                    строительная уборка промышленных <br>
                    зданий и сооружений, торговых<br>
                    центров, офисов и пр.
                </p>
            </a>
        </li>
        <li class="Services-Item">
            <a href="service-4" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-4.jpg" alt="">

                <p>
                    Нанесение защитных покрытий на все<br>
                    виды полов
                </p>
            </a>
        </li>
        <li class="Services-Item">
            <a href="service-5" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-5.jpg" alt="">

                <p>
                    Мытье окон, фасадов зданий
                </p>
            </a>
        </li>
        <li class="Services-Item">
            <a href="service-6" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-6.jpg" alt="">

                <p>
                    Промышленный альпинизм
                </p>
            </a>
        </li>
        <li class="Services-Item">
            <a href="service-7" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-7.jpg" alt="">

                <p>
                    Химическая чистка мягкой мебели,<br>
                    ковровых и напольных покрытий;<br>
                    одежды сцены с последующей <br>
                    обработкой огнезащитным составом
                </p>
            </a>
        </li>
        <li class="Services-Item">
            <a href="service-8" class="JS-Image-Align" data-image-ratio=''>
                <img src="/images/service-8.jpg" alt="">

                <p>Очистка береговой зоны с вывозом <br>мусора</p>
            </a>
        </li>

    </ul>




</div>


