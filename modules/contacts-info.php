<div class="Contacts-Info">
    <dl class="address">
        <dt>Адрес</dt>
        <dd> ул. Электрозаводская, дом 1<br>
            г. Владимир
        </dd>
    </dl>
    <dl class="tel">
        <dt>Телефоны</dt>
        <dd><a href="tel:+7(4922)37-39-11"> +7&nbsp;(4922)&nbsp;37&#8209;39&#8209;11</a></dd>
    </dl>
    <dl class="email">
        <dt>E-mail:</dt>
        <dd>
            <a href="mailto:clining@mail.ru">clining@mail.ru</a><br>
            <a href="mailto:slugba_uborki@mail.ru">slugba_uborki@mail.ru</a>
        </dd>
    </dl>
</div>