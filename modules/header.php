<div class="Header">
    <div class="Header-Content">
        <div class="Header-Block Logo">
            <a href="/" class="Header-Logo">
                <?php
                require_once $Dir_Images . "logo.svg";
                ?>
                <h1>
                    Городская<br>
                    Служба уборки
                </h1>

                <div class="Header-Back">
                    <?php
                    require_once $Dir_Images . "back.svg";
                    ?>
                    <span>Вернуться на главную</span>
                </div>
            </a>
        </div>
        <div class="Header-Block Menu">
            <nav>
                <ul>
                    <li>
                        <a href="/">
                            Главная
                        </a>
                    </li>
                    <li><a href="services">
                            Услуги
                        </a>
                    </li>
                    <li><a href="vacancies">
                            Вакансии
                        </a>
                    </li>
                    <li><a href="contacts">
                            Контакты
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <div class="Header-Block Phone">
            <div class="Header-Phone">
                <a href="tel:+7(4922)37-39-11" class="tel">
                    <?php
                    require_once $Dir_Images . "phone.svg";
                    ?>

                    <span>
                    +7&nbsp;(4922)&nbsp;37&#8209;39&#8209;11
                </span>
                </a>

                <div class="Header-Triangle"></div>
            </div>
        </div>
    </div>
</div>